<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= base_url('assets/vendors/bootstrap/css/bootstrap.min.css');?>">
        
        <!-- Fontawesome CSS -->
        <link rel="stylesheet" href="<?= base_url('assets/vendors/fontawesome/css/all.min.css');?>">

        <!-- Modal Style -->
        <style type="text/css">
            #overlay {
                position: fixed;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
                background: rgba(0, 0, 0, 0.6);
            }
        </style>

        <title>Hello, world!</title>
    </head>
    <body>
        <nav class="navbar navbar-light bg-light">
            <span class="navbar-brand mb-0 h1">Rest Server CRUD</span>
        </nav>
        <div id="app">
            <div class="container">
                <div class="row mt-3">
                    <div class="col-6">
                        <h3 class="text-info">Registered Users</h3>
                    </div>
                    <div class="col-6">
                        <button class="btn btn-info float-right" @click="showAddModal=true">
                            <i class="fas fa-user">&nbsp;&nbsp;Add New user</i>
                        </button>
                    </div>
                </div>
                
                <hr class="bg-info">
                
                <div class="alert alert-danger" v-if="errorMsg">
                    <i class="far fa-times-circle"></i> {{ errorMsg }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert alert-success" v-if="successMsg" role="alert">
                    <i class="far fa-check-circle"></i> {{ successMsg }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <!-- Table -->
                <div class="row">
                    <div class="col-12">
                        <table class="table table-bordered table-stripped">
                            <thead>
                                <tr class="text-center bg-info text-light">
                                    <th>ID</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="text-center" v-for="user in users">
                                    <td>{{ user.id }}</td>
                                    <td>{{ user.first_name }}</td>
                                    <td>{{ user.last_name }}</td>
                                    <td>
                                        <a href="#" class="text-success">
                                            <i class="fas fa-edit" @click="showEditModal=true; selectUser(user);"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="text-danger" @click="showDeleteModal=true; selectUser(user);">
                                            <i class="fas fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!-- Add user modal -->
            <div id="overlay" v-if="showAddModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Add New User</h5>
                            <button type="button" class="close" @click="showAddModal=false">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body p-4">
                            <form action="#" method="post">
                                <div class="form-group">
                                    <input type="text" name="first_name" class="form-control form-control-lg" placeholder="First name" v-model="newUser.first_name">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="last_name" class="form-control form-control-lg" placeholder="Last name" v-model="newUser.last_name">
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-info btn-block btn-lg" type="submit" @click="showAddModal=false; addUser();">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Edit user modal -->
            <div id="overlay" v-if="showEditModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Edit User</h5>
                            <button type="button" class="close" @click="showEditModal=false">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body p-4">
                            <form action="#" method="post">
                                <div class="form-group">
                                    <input type="text" name="first_name" class="form-control form-control-lg" placeholder="First name" v-model="currentUser.first_name">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="last_name" class="form-control form-control-lg" placeholder="Last name" v-model="currentUser.last_name">
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-info btn-block btn-lg" type="submit" @click="showEditModal=false; updateUser();">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Delete user modal -->
            <div id="overlay" v-if="showDeleteModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Delete User</h5>
                            <button type="button" class="close" @click="showDeleteModal=false">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body p-4">
                            <h4 class="text-danger">Are you sure want to delete this user ?</h4>
                            <h5>You are deleting {{ currentUser.first_name }}</h5>
                            <hr>
                            <button class="btn btn-danger btn-lg" type="submit" @click="showDeleteModal=false; deleteUser();">Yes</button>
                            <button class="btn btn-success btn-lg" @click="showDeleteModal=false">No</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        
        <!-- Jquery -->
        <script src="<?= base_url('assets/vendors/jquery/jquery-3.6.0.js');?>"></script>

        <!-- VueJS -->
        <script src="<?= base_url('assets/vendors/vue/vue.min.js');?>"></script>
        
        <!-- Axios -->
        <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
        
        <!-- Bootstrap JS -->
        <script src="<?= base_url('assets/vendors/bootstrap/js/bootstrap.min.js');?>"></script>

        <!-- Vue Script -->
        <script>
            var app = new Vue({
                el: '#app',
                data: {                   
                    errorMsg: '',
                    successMsg: '',
                    showAddModal: false,
                    showEditModal: false,
                    showDeleteModal: false,
                    users: [],
                    newUser: {first_name: '', last_name: ''},
                    currentUser: {}
                },
                mounted: function(){
                    this.getUsers();
                },
                methods: {
                    getUsers(){
                        axios.get('http://localhost/rest-server/api/user').then(function(response){
                            response.data.status == false ? app.errorMsg = response.data.message : app.users = response.data.message;
                        });
                    },
                    addUser(){
                        var formData = app.toFormData(app.newUser);
                        axios.post('http://localhost/rest-server/api/user', formData).then(function(response){
                            app.newUser = {first_name: '', last_name: ''};
                            if(!response.data.status){
                                app.errorMsg = response.data.message;
                            } else {
                                app.successMsg = response.data.message;
                                app.getUsers();
                            }
                        });
                    },
                    updateUser(){
                        var formData = app.toFormData(app.currentUser);
                        var json = JSON.stringify(Object.fromEntries(formData.entries()));
                        axios.patch('http://localhost/rest-server/api/user', json).then(function(response){
                            app.currentUser ={};
                            if(!response.data.status){
                                app.errorMsg = response.data.message;
                            } else {
                                app.successMsg = response.data.message;
                                app.getUsers();
                            }
                        });
                    },
                    deleteUser(){
                        var formData = app.toFormData(app.currentUser);
                        axios.post('http://localhost/rest-server/home/delete', formData).then(function(response){
                            app.currentUser ={};
                            if(!response.data.status){
                                app.errorMsg = response.data.message;
                            } else {
                                app.successMsg = response.data.message;
                                app.getUsers();
                            }
                        });
                    },
                    toFormData(obj){
                        var fd = new FormData();
                        for(var i in obj){
                            fd.append(i,obj[i])
                        }
                        return fd;
                    },
                    selectUser(user){
                        app.currentUser = user;
                    }
                }
            });
        </script>
    </body>
</html>