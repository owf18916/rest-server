<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{
	public function index()
	{
		$this->load->view('home');
	}

	public function delete()
	{
		$id = $this->input->post('id');

        if ($id === null) {
            $data = [
                'status' => false,
                'message' => 'Provide valid ID'
			];
			echo json_encode($data);
        } else {
            if ($this->user_model->deleteUser($id) > 0) {
                $data = [
					'status' => true,
					'message' => 'User has been deleted.'
				];
				echo json_encode($data);
            } else {
                $data = [
					'status' => false,
					'message' => 'ID not found.'
				];
				echo json_encode($data);
            }
        }
	}
}
