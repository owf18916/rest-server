<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class User extends REST_Controller 
{
    public function index_get()
    {
        $id = $this->get('id');
        
        if ($id === null) {
            $users = $this->user_model->getUsers();
        } else {
            $users = $this->user_model->getUsers($id);
        }
        
        if ($users) {
            $this->response([
                'status' => true,
                'message' => $users,
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => 'ID not found.'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function index_post()
    {
        $data = [
            'first_name' => $this->post('first_name'),
            'last_name' => $this->post('last_name')
        ];

        if ($this->user_model->createUser($data) > 0) {
            $this->response([
                'status' => true,
                'message' => 'User created successfully.'
            ], REST_Controller::HTTP_CREATED);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Failed to create user.'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function index_patch()
    {
        $json = $this->patch();
        print_r($json);die;
        $id = $this->patch('id');

        $data = [
            'first_name' => $this->patch('first_name'),
            'last_name' => $this->patch('last_name')
        ];

        if ($this->user_model->updateUser($data, $id) > 0) {
            $this->response([
                'status' => true,
                'message' => 'User updated successfully.'
            ], REST_Controller::HTTP_NO_CONTENT);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Failed to update.'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function index_delete()
    {
        $id = $this->delete('id');

        if ($id === null) {
            $this->response([
                'status' => false,
                'message' => 'Provide valid ID'
            ], REST_Controller::HTTP_BAD_REQUEST);
        } else {
            if ($this->user_model->deleteUser($id) > 0) {
                $this->response([
                    'status' => true,
                    'message' => 'User ID ' . $id . ' deleted successfully.'
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'ID not found.'
                ], REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }
}
